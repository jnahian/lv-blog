@extends('layouts.master')

@section('title') {{$post->title}} @stop()

@section('content')

@if($post)
<div class="row">
    <div class="col-sm-12">
        @if($post->attachment)
        <img src="{{asset("uploads/posts/$post->user_id/$post->attachment")}}" class="img-responsive img-thumbnail" alt="" />
        @endif
    </div>


    <div class="col-sm-12">
        <h4><a href="{{route('post.show', $post->id)}}">{{$post->title}}</a></h4>
        <p>
            {{{$post->content}}}
        </p>

        <div class="col-sm-6">
            <h4>All Comments</h4>
            <ul class="list-unstyled">
                @foreach($post->comments as $comment)
                    <li>
                        <a href="#">{{$comment->user->username}}, <i>On: {{$comment->created_at}}</i></a>
                        <p>{{$comment->comment}}</p>
                    </li>
                @endforeach

            </ul>
        </div>
        <div class="col-sm-6">
            <h4>Comment</h4>
            {{Form::open(['route' => ['post.comment', $post->id]])}}
            {{Form::textarea('comment', null, ['class'=>'form-control', 'placeholder'=>'Your Comment', 'required' => true])}}
            <span class="text-danger">{{$errors->first()}}</span>
            {{Form::submit('Send', ['class'=>'btn btn-success pull-right'])}}
            {{Form::close()}}
        </div>
    </div>


</div>
@endif

@stop()