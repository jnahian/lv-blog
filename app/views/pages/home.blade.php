@extends('layouts.master')

@section('title') Home @stop()

@section('content') 

@if($posts)
    @foreach($posts as $post)
    
    <div class="row">
        <div class="col-sm-4">
            @if($post->attachment)
                <img src="{{asset("uploads/posts/$post->user_id/$post->attachment")}}" class="img-responsive img-thumbnail" alt="" />
            @endif
        </div>
        <div class="col-sm-8">
            <h4><a href="{{route('post.show', $post->id)}}">{{$post->title}}</a></h4>
            <i class="text-muted">On: {{$post->created_at}}, By: {{$post->user['last_name']}}</i>
            <p>
                {{str_limit($post->content, 500, '...')}}
            </p>
            {{ link_to(route('post.show', $post->id), 'Details') }}
            <ul class="list-inline">
                @foreach($post->tags as $tag)
                <li class="label label-info">{{$tag->name}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endforeach
    
    {{$posts->links()}}
    
@endif

@stop()