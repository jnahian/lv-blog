<?php

class PostController extends \BaseController {

    /**
     *
     */
    public function __construct(Post $post, Tag $tag, User $user, Comment $comment) {
        $this->post = $post;
        $this->tag = $tag;
        $this->user = $user;
        $this->comment = $comment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $posts = $this->post->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->with(['tags', 'user'])->paginate(5);
        return View::make('pages.posts')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $tags = $this->tag->get();
        return View::make('pages.create_post')->withTags($tags);
    }

    /**
     * Store a newly created post in database.
     *
     * @return Response
     */
    public function store() {
        $input = Input::all();
        if ($this->post->fill($input)->validate_post()) {
            $image = Input::file('attachment');
            if ($image->isValid()) {
                $path = 'uploads/posts/' . Auth::id();
                $filename = 'posts-' . time() . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                if ($image->move($path, $filename)) {
                    $data = $this->post->create([
                        'user_id' => Auth::user()->id,
                        'title' => $input['title'],
                        'content' => $input['content'],
                        'attachment' => $filename,
                    ]);
                    if ($data->id) {
                        $post = $this->post->find($data->id);
                        $post->tags()->attach($input['tags']);
                        Session::flash('type', 'success');
                        Session::flash('message', 'Post Created');
                        return Redirect::route('post.index');
                    } else {
                        Session::flash('type', 'danger');
                        Session::flash('message', 'Error!!! Cannot create post');
                        return Redirect::back()->withInput();
                    }
                } else {
                    Session::flash('type', 'danger');
                    Session::flash('message', 'Error!!! File cannot be uploaded');
                    return Redirect::back()->withInput();
                }
            } else {
                Session::flash('type', 'danger');
                Session::flash('message', 'Error!!! File is not valid');
                return Redirect::back()->withInput();
            }
        } else {
            return Redirect::back()->withInput()->withErrors($this->post->errors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $post = $this->post->find($id);
        return View::make('pages.post_details')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $post = $this->post->find($id);
        return View::make('pages.edit_post')->with(['post' => $post, 'tags' => $this->tag->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $input = Input::all();
        if ($this->post->fill($input)->validate_post()) {
            $image = Input::file('attachment');
            $post = $this->post->find($id);
            if ($image->isValid()) {

                $path = 'uploads/posts/' . ( $this->user->isAdmin() ? Auth::id() : $post->user_id );

                $filename = 'posts-' . time() . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                if ($image->move($path, $filename)) {
                    $post->title = Input::get('title');
                    $post->content = Input::get('content');
                    $post->attachment = $filename;
                    $post->approved = 0;
                    if ( $post->save() ) {
                        $post->tags()->sync($input['tags']);
                        Session::flash('type', 'success');
                        Session::flash('message', 'Post Updated');
                        return Redirect::route('post.index');
                    } else {
                        Session::flash('type', 'danger');
                        Session::flash('message', 'Error!!! Cannot update post');
                        return Redirect::back()->withInput();
                    }
                } else {
                    Session::flash('type', 'danger');
                    Session::flash('message', 'Error!!! File cannot be uploaded');
                    return Redirect::back()->withInput();
                }
            } else {
                Session::flash('type', 'danger');
                Session::flash('message', 'Error!!! File is not valid');
                return Redirect::back()->withInput();
            }
        } else {
            return Redirect::back()->withInput()->withErrors($this->post->errors);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        try {
            $post = $this->post->find($id);
            if ($post->delete()) {
                Session::flash('type', 'success');
                Session::flash('message', 'Post Deleted');
                return Redirect::back();
            }
        } catch (Exception $ex) {
            echo 'Error!';
        }
    }

    /**
     * Approve post for publishing
     *
     * @return response
     */
    public function approve($id) {
        $post = $this->post->find($id);
        $post->approved = '1';
        if ($post->save()) {
            Session::flash('type', 'success');
            Session::flash('message', 'Post Approved');
            return Redirect::back();
        }
        Session::flash('type', 'danger');
        Session::flash('message', 'Error!!! Post not Approved');
        return Redirect::back();
    }

    /**
     * Make comment to a post
     *
     * @return response
     */
    public function comment($postid) {
        try {
            $input = Input::all();
            if ($this->comment->fill($input)->validate_comment()) {
                $comment = new Comment(['comment' => $input['comment'], 'user_id' => Auth::id()]);
                $post = $this->post->find($postid);
                $post->comments()->save($comment);

                Session::flash('type', 'success');
                Session::flash('message', 'Commented Successfully');
                return Redirect::back();
            } else {
                return Redirect::back()->withErrors($this->comment->errors);
            }
        } catch (Exception $ex) {
            return Redirect::back()->withErrors($this->comment->errors);
        }
    }

}
