<?php

class HomeController extends BaseController {
    
    public function __construct(Post $post, Tag $tag, User $user) {
        $this->post = $post;
        $this->tag = $tag;
        $this->user = $user;
    }

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/
    
    public function index() 
    {
        $posts = $this->post->where('approved', '1')->orderBy('id', 'desc')->with(['tags', 'user'])->paginate(2);
        return View::make('pages.home')->with('posts', $posts);
    }
    

}
