<?php

class AdminController extends \BaseController {

    public function __construct(Post $post, Tag $tag, Role $role) {
        $this->post = $post;
        $this->tag = $tag;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if(Auth::user()->isAdmin()){
            $posts = $this->post->orderBy('id', 'desc')->with('tags')->paginate(5);
        } else {
            $posts = $this->post->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->with('tags')->paginate(5);
        }
        return View::make('pages.posts')->with('posts', $posts);
    }

}
