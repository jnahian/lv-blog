<?php

class Comment extends Eloquent {

    /**
     * The fillable fields of comments table
     *
     * @var array
     */
    protected $fillable = ['comment', 'user_id'];

    /**
     * The rules for validating comment
     *
     * @var array
     */
    protected $rules = ['comment' => 'required'];

    /**
     * The error messages
     *
     * @var array
     */
    protected $errors = [];

    /**
     * The method for validating comment
     *
     * @return boolean
     */
    public function validate_comment() {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->passes()) {
            return TRUE;
        }
        $this->errors = $validation->messages();
        return FALSE;
    }

    /**
     * One to many inverse relation with posts table
     * 
     * @return response
     */
    public function user() {
        return $this->belongsTo('User');
    }

}
