<?php

/*
|
|   Home Routes
|
 */

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('show', ['as' => 'show', 'uses' => 'HomeController@show']);

/*
|
|   Admin Routes
|*/

Route::get('login', ['as' => 'login', 'uses' => 'UserController@login_form']);
Route::post('admin/login/process', ['as' => 'login.process', 'uses' => 'UserController@login_process']);
Route::get('signup', ['as' => 'signup', 'uses' => 'UserController@create']);
Route::post('signup/process', ['as' => 'signup.process', 'uses' => 'UserController@store']);

Route::group(['before' => 'auth'], function () {
	Route::get('admin', ['as' => 'admin', 'uses' => 'AdminController@index']);
	Route::group(['before' => 'role'], function () {
		Route::resource('admin/user', 'UserController', ['except' => ['create', 'store']]);
		Route::get('admin/user/{user}/approve', ['as' => 'user.approve', 'uses' => 'UserController@approve']);
	});
	Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
	Route::get('admin/{user}/delete', ['as' => 'user.delete', 'uses' => 'UserController@destroy']);
	Route::get('post/{id}/delete', ['as' => 'post.delete', 'uses' => 'PostController@destroy']);
	Route::get('post/{id}/approve', ['as' => 'post.approve', 'uses' => 'PostController@approve']);
	Route::post('post/{id}/comment', ['as' => 'post.comment', 'uses' => 'PostController@comment']);
});
Route::resource('post', 'PostController');