-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2016 at 11:44 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lv_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(2, 18, 2, 'asfgasfsafsaflsdplmasphmsdfphpahsdhsdfh', '2016-02-02 00:28:32', '2016-02-02 00:28:32'),
(3, 18, 2, 'asfgasfsafsaflsdplmasphmsdfphpahsdhsdfh', '2016-02-02 00:29:42', '2016-02-02 00:29:42'),
(4, 18, 2, 'jsdkjkasdgjbvjasdbvl\r\n', '2016-02-02 00:55:28', '2016-02-02 00:55:28'),
(5, 18, 2, 'Hey Man What''s Up!\r\n', '2016-02-02 00:55:51', '2016-02-02 00:55:51'),
(6, 18, 2, 'sdagdsgsbsdavsdavsd', '2016-02-02 00:57:57', '2016-02-02 00:57:57'),
(7, 18, 2, 'asgsdgsavsdsadvsdv', '2016-02-02 01:06:19', '2016-02-02 01:06:19'),
(8, 18, 2, 'gasgasgdsgfsdagsdg', '2016-02-02 01:07:18', '2016-02-02 01:07:18'),
(9, 17, 22, 'gbdajskgbsdajkvsad\r\n', '2016-02-02 01:37:47', '2016-02-02 01:37:47'),
(10, 16, 2, 'সাইফউদ্দিন নাকি নিজের ইচ্ছেমতো ইয়র্কার দিতে পারে?', '2016-02-02 23:50:06', '2016-02-02 23:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_01_25_092824_create_users_table', 1),
('2016_01_25_093904_create_roles_table', 1),
('2016_01_25_094136_create_role_user_table', 1),
('2016_01_25_094205_create_posts_table', 1),
('2016_01_25_094306_create_tags_table', 1),
('2016_01_25_094320_create_post_tag_table', 1),
('2016_01_25_101420_create_comments_table', 1),
('2016_01_26_110017_create_role_user_trigger', 2),
('2016_02_04_065528_add_id_to_post_tag_table', 3),
('2016_02_04_071115_add_increments_to_post_tags_table', 4),
('2016_02_05_065254_add_approved_column_to_users_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `content`, `attachment`, `approved`, `created_at`, `updated_at`) VALUES
(12, 2, 'সবাইকে ছাড়িয়ে শীর্ষে এখন মিরাজ', 'শ্বাসরুদ্ধকর উত্তেজনা! মেহেদী হাসান মিরাজের একেকটা বল যায়, আর দর্শকদের স্নায়ুর ওপর দিয়ে যেন ঝড় যায়! ভাবছেন, শুরু থেকেই কাঁপতে থাকা নামিবিয়ার বিপক্ষে আবার ‘শ্বাসরুদ্ধকর’ পরিস্থিতি হলো কোথায়!\r\n\r\n\r\nনামিবিয়ার তখন শেষ উইকেট। ৩৩তম ওভারে বোলিংয়ে এলেন মিরাজ। এর আগে ৭ ওভার বোলিং করে পেয়েছেন ১ উইকেট। আর তাতেই যুব ওয়ানডেতে পাকিস্তানের ইমাদ ওয়াসিমের সর্বোচ্চ ৭৩ উইকেটে ভাগ বসিয়েছেন। ইমাদকে ছাড়িয়ে যেতে যুবা অধিনায়কের দরকার আর একটি উইকেট। নামিবিয়া অনূর্ধ্ব-১৯ দলের ইনিংস তখন শেষ দিকে। তবে কি আজ গড়া হবে না মিরাজের রেকর্ড? নাকি হয়ে যাবে আজই!\r\n\r\nএমনিতে মিরাজ মানসিকভাবে খুবই শক্ত এবং ইতিবাচক। এর আগে বোলিংয়ের রেকর্ডটা নিয়ে যখনই জিজ্ঞেস করা হয়েছে আত্মবিশ্বাসী কণ্ঠে শুধু বলেছেন ‘হয়ে যাবে’। হয়েও গেল। ওভারের পঞ্চম বলে নামিবিয়ার শেষ উইকেট ফন উইককে এলবিডব্লু করে ইমাদের ৭৩ উইকেট টপকে মিরাজ হয়ে গেলেন যুব ওয়ানডেতে সর্বোচ্চ উইকেট শিকারি।\r\n\r\nশেখ কামাল আন্তর্জাতিক স্টেডিয়ামে নিজেদের আগের ম্যাচেই পাকিস্তান ওপেনার সামি আসলামকে টপকে মিরাজের সতীর্থ নাজমুল হোসেন শান্ত হয়েছিলেন যুব ওয়ানডেতে সর্বোচ্চ রানের নতুন মালিক। সেই দিন মিরাজের সামনেও সুযোগ ছিল বোলিংয়ের রেকর্ডটি গড়ার। তবে তাঁকে একটু অপেক্ষায় রেখেছিল শেখ কামাল আন্তর্জাতিক স্টেডিয়াম।\r\n\r\nসেই অপেক্ষার অবসান হলো আজ। মিরাজ-ইমাদের মধ্যে অবশ্য ম্যাচ সংখ্যায় খুব একটা পার্থক্য নেই। ২০০৫ থেকে ২০০৮—এই সময় পাকিস্তান অনূর্ধ্ব-১৯ দলের হয়ে ইমাদ খেলেছেন ৪৯ ম্যাচ। সেখানে মিরাজ ৫৩ ম্যাচে উঠলেন চূড়ায়।\r\n\r\nবোলিংয়ে দুর্দান্ত হলে ব্যাটিংয়েও কিন্তু খুব বেশি পিছিয়ে নেই। ১১৩৭ রান করেছেন ব্যাট হাতে। যুব আন্তর্জাতিক ওয়ানডেতে সর্বোচ্চ রানের মালিকদের তালিকায় আছেন ১২ নম্বরে।\r\n\r\nহোক না যুব ক্রিকেট। কোনো একটা আন্তর্জাতিক ক্রিকেটের পাতায় সর্বোচ্চ রান আর সর্বোচ্চ উইকেট—দু​টিই বাংলাদেশের দখলে, এ তো অনেক গর্বের।', 'posts-14546462161480.jpg', 1, '2016-01-30 02:16:33', '2016-02-04 22:24:03'),
(13, 2, 'নেপাল অনূর্ধ্ব ১৯ অধিনায়কের বয়স ২৫ বছর!', 'এবারের যুব বিশ্বকাপে চমক দেখিয়ে নজর কেড়েছে নেপাল। নিউজিল্যান্ড-আয়ারল্যান্ডকে হারিয়ে উঠে গেছে কোয়ার্টার ফাইনালে। আলাদা করে নজর কেড়েছেন দলটির অধিনায়ক রাজু রিজল। চারদিকে নেপাল আর রিজলকে নিয়ে যখন প্রশস্তিগাথা, সেই সময় উঠল গুরুতর অভিযোগ। মুম্বাইয়ের রঞ্জি খেলা এক ক্রিকেটার প্রশ্ন তুলেছেন, রিজল এই বিশ্বকাপ খেলছেন কী করে। তাঁর বয়স ১৯ কিংবা এর কম হওয়ার প্রশ্নই আসে না। কৌস্তুব পাওয়ার নামের সেই ক্রিকেটারের দাবি, রিজলের বয়স ২৪-২৫ বছরের মধ্যে! শুধু বয়স নয়, রিজল নামও লুকিয়েছেন বলে দাবি পাওয়ারের!\r\nকাল ভারতের সঙ্গে ম্যাচ ছিল নেপালের। এমন সময় ফেসবুকে পাওয়ার এই অভিযোগ এনে একটি স্ট্যাটাস দিয়েছেন। সেখানে হ্যাশ ট্যাগ ব্যবহার করে পাওয়ার লিখেছেন: ‘#সেভক্রিকেট’, ‘#শেম’। একসঙ্গে মুম্বাইয়ের ক্রিকেটে খেলেছেন দাবি করে পাওয়ারের বক্তব্য, ‘মুম্বাইয়ের হয়ে আমরা অনূর্ধ্ব ১৫ ক্রিকেটে এক সঙ্গে খেলেছি। আর এখন ও নেপালের অনূর্ধ্ব ১৯ দলের অধিনায়ক! অথচ আমাদের সে দলের সবার​ বয়স এখন ২৪-২৫। মুম্বাইয়ে সে রাজু শর্মা, আর নেপালে গিয়ে রাজু রিজল! ক্রিকেটকে বাঁচান। কী লজ্জার!’\r\nপ্রথম ম্যাচে নিউজিল্যান্ডকে হারিয়ে আলোচনায় আসে নেপাল ও দলটির অধিনায়ক। সেই ম্যাচের সেরা খেলোয়াড় হয়েছিলেন রিজল। দলটির সেরা ব্যাটসম্যান, উইকেটরক্ষকও তিনি। যেন এমএস ধোনির নেপালি সংস্করণ। দলটির ম্যানেজার রিজলকে ‘ক্যাপটেন কুল’ হিসেবেও আখ্যা দেন মিডিয়ায়। রিজলের অফিশিয়াল রেকর্ড অনুয়ায়ী তিনি ১৯৯৬ সালের ২৬ সেপ্টেম্বর নেপালের ধাঙ্গা​ড়িতে জন্মেছেন।\r\nএমন অভিযোগের ব্যাপারে রিজলের বক্তব্য, ‘এ ব্যাপারে আমার কোনো ধারণাই নেই। আমি সোশ্যাল মিডিয়ায় নেই। এ ব্যাপারে তাই কোনো মন্তব্য করতে পা​রছি না।’\r\nরিজল অভিযোগটি এড়িয়ে গেলেও এ নিয়ে ফেসবুকে কিন্তু ভালোই শোরগোল চলছে। দেখা যাক আইসিসি এ ব্যাপারে কী পদক্ষেপ নেয়। বয়স ভাড়িয়ে এ ধরনের টুর্নামেন্ট খেলার অভিযোগ অবশ্য পুরোনো। যদিও বয়স লুকানোর সেই প্রবণতা এখন অনেকটাই কমে এসেছে।', 'posts-14546462017718.jpg', 1, '2016-01-31 22:23:54', '2016-02-04 22:23:57'),
(15, 22, 'laravel route filter to check user roles', ' 10\r\ndown vote\r\naccepted\r\n	\r\n\r\nTry the following:\r\n\r\nfilters.php\r\n\r\nRoute::filter(''role'', function()\r\n{ \r\n  if ( Auth::user()->role !==1) {\r\n     // do something\r\n     return Redirect::to(''/''); \r\n   }\r\n}); \r\n\r\nroutes.php\r\n\r\n Route::group(array(''before'' => ''role''), function() {\r\n        Route::get(''customer/retrieve/{id}'', ''CustomerController@retrieve_single'');\r\n        Route::post(''customer/create'', ''CustomerController@create'');\r\n        Route::put(''customer/update/{id}'', ''CustomerController@update'');\r\n\r\n\r\n});\r\n\r\n', 'posts-14543115743346.jpg', 1, '2016-02-01 01:26:14', '2016-02-04 22:23:56'),
(16, 2, 'টিভিতে দেখা যাবে মিরাজদের খেলা', 'আচ্ছা, মিরাজ কেমন বল করে? কখনো সরাসরি খেলতে দেখিনি ছেলেটাকে’, ‘সাইফউদ্দিন নাকি নিজের ইচ্ছেমতো ইয়র্কার দিতে পারে? কেউ দেখেছে ওর খেলা?’ কিংবা ‘শুধু শুনেই গেলাম, দেখতে পারলাম না নাজমুল কেমন ব্যাটিং করে?’ হতাশামাখা প্রশ্নগুলো গত কয়েক দিন ধরেই ঘুরছিল বাংলাদেশের ক্রিকেটপ্রেমীদের মুখে।\r\n\r\n\r\nঘোরারই কথা! ঘরের মাটিতে বিশ্বকাপ হচ্ছে, তাতে বাংলাদেশকে বিশ্বকাপের জন্য সবচেয়ে ফেবারিটদের একটি। অথচ গ্রুপপর্বের বাংলাদেশের ৩ ম্যাচের মধ্যে মাত্র একটি সরাসরি দেখতে পেরেছে বাংলাদেশের মানুষ!\r\n\r\nঅনূর্ধ্ব-১৯ দল, যাদের নিয়ে এত উচ্চাশা, তারা কেমন খেলে সেটি নিজের চোখে দেখার একটা ইচ্ছে তো জাগারই কথা। সবচেয়ে বড় কথা, এত এতবার লেখা হয়েছে এদের নাম, কালো অক্ষরে শুধু দেখে কি আর আশ মেটে?\r\n\r\nমাঠে গিয়ে দেখা বেশির ভাগ মানুষের পক্ষে সম্ভব নয়। বিকল্প উপায় টিভিতে দেখা। গ্রুপপর্বে বাংলাদেশের মাত্র একটি ম্যাচ সরাসরি টিভিতে দেখিয়েছে। বাকি দুটি দেখায়নি। ক্রিকেটপ্রেমীদের জন্য সুখবর হলো, বাংলাদেশের কোয়ার্টার ফাইনালটি সরাসরি সম্প্রচার করা হবে। যেখানে আগামী ৫ ফেব্রুয়ারি বাংলাদেশের প্রতিপক্ষ গ্রুপ ডি রানার্সআপ নেপাল। ম্যাচটি সরাসরি দেখাবে বিটিভি, মাছরাঙা টিভি, জি টিভি, ও স্টার স্পোর্টস ১।\r\n\r\nএমনিতে এবারই সবচেয়ে বেশি যুব পর্যায়ের বিশ্বকাপের খেলা সরাসরি সম্প্রচার করা হচ্ছে। কিন্তু দুর্ভাগ্যবশত সম্প্রচার তালিকায় থাকা ম্যাচগুলোর বেশির ভাগেই বাংলাদেশ ছিল না। শুধু চট্টগ্রামের জহুর আহমেদ চৌধুরী স্টেডিয়াম ও মিরপুর শের-এ বাংলা জাতীয় স্টেডিয়ামে অনুষ্ঠিত ম্যাচগুলোই দেখাচ্ছে সম্প্রচার কর্তৃপক্ষ। এই দুটি ভেন্যুর মধ্যে বাংলাদেশের ম্যাচ শুধু গ্রুপের প্রথম ম্যাচটিই খেলেছে চট্টগ্রামে, দক্ষিণ আফ্রিকার বিপক্ষে। বাকি দুটি ম্যাচ বাংলাদেশ খেলেছে কক্সবাজারের শেখ কামাল আন্তর্জাতিক ক্রিকেট স্টেডিয়ামে, যেখান থেকে কোনো ম্যাচই সম্প্রচার করা হয়নি।\r\n\r\nকোয়ার্টার ফাইনাল তো দেখাবেই, পাশাপাশি সেমিফাইনাল ও ফাইনালও সম্প্রচারিত হবে। মিরাজ​রা সেমিফাইনাল ও ফাইনাল খেললে কোয়ার্টার ফাইনালের পর দেখা যাবে আরও দুই ম্যাচ।', 'posts-14546461917521.jpg', 1, '2016-02-01 02:02:22', '2016-02-04 22:23:54'),
(17, 2, 'দ্বৈত নাগরিকত্বের পরিসর বাড়ল বাংলাদেশিদের', '    প্রচ্ছদ\r\n    বাংলাদেশ\r\n    সরকার\r\n\r\nদ্বৈত নাগরিকত্বের পরিসর বাড়ল বাংলাদেশিদের\r\nবিশেষ প্রতিনিধি | আপডেট: ১৫:২৯, ফেব্রুয়ারি ০১, ২০১৬\r\n২ Like\r\n৪\r\n \r\n \r\n \r\n \r\n \r\n \r\n\r\nপ্রধানমন্ত্রী শেখ হাসিনার সভাপতিত্বে তাঁর কার্যালয়ে মন্ত্রিসভার বৈঠক অনুষ্ঠিত হয়। ওই বৈঠকে বাংলাদেশি নাগরিকদের জন্য দ্বৈত নাগরিকত্বের পরিসর সম্প্রসারিত করা সংক্রান্ত বাংলাদেশ নাগরিকত্ব আইন, ২০১৬ চূড়ান্ত অনুমোদন দিয়েছে মন্ত্রিসভা। ছবি: ফোকাস বাংলাবাংলাদেশি নাগরিকদের জন্য দ্বৈত নাগরিকত্বের পরিসর সম্প্রসারিত করা হয়েছে। তবে সরকারি চাকরিতে থাকা ব্যক্তিরা এ সুযোগ নিতে পারবেন না। আজ সোমবার এ-সংক্রান্ত বাংলাদেশ নাগরিকত্ব আইন, ২০১৬–এর খসড়ার চূড়ান্ত অনুমোদন দিয়েছে মন্ত্রিসভা।\r\n\r\nপ্রধানমন্ত্রী শেখ হাসিনার সভাপতিত্বে তাঁর কার্যালয়ে এ বৈঠক অনুষ্ঠিত হয়।\r\n\r\nবৈঠক শেষে মন্ত্রিপরিষদ সচিব মোহাম্মদ শফিউল আলম সাংবাদিকদের ব্রিফ করেন। তিনি বলেন, দ্বৈত নাগরিকত্ব গ্রহণের বিষয়টি অনেক সম্প্রসারিত হয়েছে। আগে যুক্তরাজ্য ও যুক্তরাষ্ট্র এই দুটি দেশের ক্ষেত্রে দ্বৈত নাগরিকত্বের সুযোগ ছিল। নতুন আইনে বলা হয়েছে, সার্কভুক্ত দেশ বা মিয়ানমার বা সরকার কর্তৃক গেজেট প্রজ্ঞাপন দ্বারা নাগরিকত্ব গ্রহণে নিষিদ্ধঘোষিত রাষ্ট্র ছাড়া বাংলাদেশের সঙ্গে কূটনৈতিক সম্পর্ক রয়েছে—এমন যেকোনো রাষ্ট্রের দ্বৈত নাগরিকত্ব গ্রহণ করতে পারবেন বাংলাদেশিরা।\r\nমন্ত্রিপরিষদ সচিব জানান, তবে কেউ সুপ্রিম কোর্টের বিচারক, এমপি বা সাংবিধানিক পদে অধিষ্ঠিত বা শৃঙ্খলা বাহিনীতে বা প্রজাতন্ত্রের বেসামরিক পদে নিয়োজিত থাকাকালে দ্বৈত নাগরিকত্ব গ্রহণ করতে পারবেন না।\r\n\r\n', 'posts-14546461776547.jpg', 1, '2016-02-01 05:13:27', '2016-02-04 22:23:52'),
(18, 22, 'আন্তর্জাতিক ক্রিকেট  হুমকির জবাবে নামিবিয়াকে ক্রিকেট শেখালেন ​মিরাজরা!', '    প্রচ্ছদ\r\n    খেলা\r\n    আন্তর্জাতিক ক্রিকেট\r\n\r\nহুমকির জবাবে নামিবিয়াকে ক্রিকেট শেখালেন ​মিরাজরা!\r\nক্রীড়া প্রতিবেদক | আপডেট: ১২:৩৩, ফেব্রুয়ারি ০২, ২০১৬\r\n৭ Like\r\n২৬\r\n \r\n \r\n \r\n \r\n \r\n \r\n\r\nনামিবিয়ার আরেকটি উইকেট পড়ার পর বাংলাদেশের যুবাদের উল্লাস। ছবি: শামসুল হক\r\n\r\nদক্ষিণ আফ্রিকাকে তারা হারিয়েছে রীতিমতো বলে-কয়েই। অনূর্ধ্ব-১৯ বিশ্বকাপের বর্তমান চ্যাম্পিয়নদের হারিয়ে শেষ আটও নিশ্চিত হয়ে গেছে নামিবিয়ার। বাংলাদেশের দিকেও চোখ রাঙানি দিচ্ছিল দলটি। কিন্তু স্বাগতিকদের বিপক্ষে মাঠে নামার কিছুক্ষণের মধ্যেই হুমকি-টুমকি ভুলে উল্টো মান বাঁচানোর লড়াইয়ে নামতে হয়েছে তাদের। বাংলাদেশের যুবারা যেন নামিবিয়াকে ক্রিকেটের প্রথম ধারাপাতই শেখাল!\r\nনামিবিয়াকে ৬৫ রানে অলআউট করে দেওয়ার পর ম্যাচ জিতে নিল মাত্র ১৬ ওভারে। ৮ উইকেট আর ২০৪ বল হাতে রেখে এল এই জয়। অবশ্য বলের ব্যবধানে এটি বাংলাদেশরও সবচেয়ে বড় জয় নয়। নামিবিয়াকেই ২০০০ যুব বিশ্বকাপে হারাতে বাংলাদেশের খরচ হয়েছিল মাত্র ১১.১ ওভার।\r\nদাপটের সঙ্গে তিন ম্যাচ জিতেই কোয়ার্টার ফাইনালে পা রাখল বাংলাদেশ। সেখানে প্রতিপক্ষ নেপাল। অবশ্য আজকের জয়ে সামান্য অতৃপ্তি থাকতে পারে ১৩ রানে দুই ওপেনারকে হারিয়ে ফেলা। জয়রাজের অপরাজিত ৩৪ ও নাজমুলের অপরাজিত ১৪ বাংলাদেশকে অনায়াসে নিয়ে গেছে জয়ের বন্দরে।\r\nআজকের ম্যাচে নামিবিয়াকে দুটি লজ্জা অবশ্য দিয়েছে বাংলাদেশ। নামিবিয়াকে সবচেয়ে কম রানে অলআউট করে দেওয়ার পর এবারের আসরের সবচেয়ে বড় ব্যবধানের জয়টিও তুলে নিয়েছে।\r\n২ উইকেটে ৩৭ তোলার পর নামিবিয়া পথ হারায়। সেই পথের দিশা আর খুঁজে পায়নি। নিয়মিত বিরতিতে উইকেট হারিয়েছে। মাত্র ২৮ রানে হারিয়েছে শেষ আট উইকেট। টসে জিতে ফিল্ডিং বেছে নেওয়া বাংলাদেশ অনূর্ধ্ব-১৯ দলের বোলারদের দাপটে দিশেহারা হয়ে পড়ে দক্ষিণ আফ্রিকার প্রতিবেশী দেশটি। বাংলাদেশের ৬ বোলারের চারজনেরই ইকোনমি রেট দুইয়েরও নিচে। এর মধ্যে সালেহ আহমেদ শাওন, আরিফুল ইসলাম ও মেহেদী মিরাজ নিয়েছেন দুটি করে উইকেট। সাইফুদ্দিন ও সাঈদ নিয়েছেন একটি করে।\r\nআগের ম্যাচে যুব আন্তর্জাতিক ওয়ানডেতে সর্বোচ্চ রানের মালিক হয়েছেন নাজমুল। এবার বাংলাদেশ অধিনায়ক মিরাজ অনূর্ধ্ব-১৯ পর্যায়ে সর্বোচ্চ উইকেটের নতুন রেকর্ড গড়েছেন।\r\nরেকর্ড গড়েই জয়যাত্রা অব্যাহত আছে মিরাজদের। সেই যাত্রার লক্ষ্য একটাই বিন্দুতে—১৪ ফেব্রুয়ারি দেশবাসীকে ভালোবাসা দিবসের সবচেয়ে বড় উপহারটি দেওয়া!', 'posts-14545703023243.jpg', 1, '2016-02-01 05:28:52', '2016-02-04 22:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`) VALUES
(1, 18, 1),
(3, 18, 2),
(4, 18, 4),
(5, 17, 1),
(6, 17, 2),
(7, 17, 3),
(8, 16, 4),
(9, 16, 5),
(10, 13, 1),
(11, 13, 2),
(12, 13, 3),
(13, 12, 1),
(14, 12, 2),
(15, 12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Author');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(7, 2, 1),
(8, 21, 2),
(10, 22, 2),
(12, 23, 2),
(14, 24, 2),
(15, 25, 2),
(16, 26, 2),
(18, 27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'Web Design'),
(2, 'Graphic Design'),
(3, 'SEO'),
(4, 'AutoCAD'),
(5, 'Web Development');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `approved`) VALUES
(2, 'jnahian', 'Julkar Naen', 'Nahian', 'info@jnahian.com', '$2y$10$4tekBpaaySGjWL/LW6rcV.ZFi1sZNBRS/qzRsePOmzFgbMLhVm2xC', 'eP5mmPK4YlUaqezWLSvL2WzYnzUSbr1Qhvm9JXyeKTQwasap3oghPwahusGl', '2016-01-26 01:09:06', '2016-02-05 04:41:47', 1),
(21, 'partha-roy', 'Partha Protim', 'Roy', 'partha_roy@gmail.com', '$2y$10$jVBeHhp1zSolkuvc8rPGTuNm2vSKJsgCmz5uE.Rm8CnuApM6NFZ4i', '9rkOpiUy1XAzcLEEKuWMAXkbmeeE7JIZP7WT7C6z', '2016-01-31 23:39:58', '2016-02-05 02:39:56', 1),
(22, 'ahabib', 'Ahsan', 'Habib', 'habib@gmail.com', '$2y$10$2gbvFjCCLqrmMONF1TeRXO1JbK8MfGfvXQhKnTxUaks7Nv2q.rPam', 'XQr7TyeIgXqOludHxUqgNp2RsKK9Sz2KJSSVGef5jobySvRcopHS9DD9SHAU', '2016-01-31 23:42:09', '2016-02-02 03:23:46', 0),
(23, 'abs_uzzal', 'ABS', 'Uzzal', 'abs@gmail.com', '$2y$10$r7TcHh.lAnyTC9OGzovmuer/y9CyMzSnRrcyD/8DBSwYIFm2t2yzG', '', '2016-02-05 02:45:38', '2016-02-05 02:45:38', 0),
(24, 'abas_uzzal', 'ABS', 'Uzzal', 'abas@gmail.com', '$2y$10$FMdOusyRk10hBt.NrtjgIedLgL7bdAIC9e0qquqpzA3IGpjKd5ZjO', '', '2016-02-05 02:48:08', '2016-02-05 02:48:08', 0),
(25, 'siraj123', 'Sirajul', 'Islam', 'siraj123@yahoo.com', '$2y$10$w6M0/ucN4Xm5TmKCYAgt9.oAT6Pa3OXlHMqMfJ2bQKjOrZjq4WugK', '', '2016-02-05 02:49:12', '2016-02-05 02:49:12', 0),
(26, 'siraj1231', 'Sirajul', 'Islam', 'siraj1231@yahoo.com', '$2y$10$q9QKrsu7trjjbdp2oVjKLeV2i4gVOeliz6BHmbUKgVlf07PXZ6EJ2', '', '2016-02-05 02:50:25', '2016-02-05 02:50:25', 0),
(27, 'fuad_alam', 'Fuad', 'Alam', 'fuad_alam@yahoo.com', '$2y$10$7qqrHjKDSL6JL5HWWlJc7eEVKMdNMaJjAo0w1c1mIPANPMzQnsvPi', '', '2016-02-05 02:55:48', '2016-02-05 02:55:48', 0);

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `tr_User_Default_Member_Role` AFTER INSERT ON `users`
 FOR EACH ROW BEGIN
                       INSERT INTO role_user (`role_id`, `user_id`) VALUES (2, NEW.id);
                   END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`), ADD KEY `comments_post_id_foreign` (`post_id`), ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`), ADD KEY `post_tag_post_id_foreign` (`post_id`), ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`), ADD KEY `role_user_role_id_foreign` (`role_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_username_unique` (`username`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
ADD CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
